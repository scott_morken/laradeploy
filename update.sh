#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"

    source ./_setup.sh
fi

echo -e "${GREEN}Upgrade${NORMAL}"

NEWRELEASENAME=${CURRDATETIME}

source ${WORKINGDIR}/_prep_upgrade.sh

echo "Updating application in ${DIRECTORY}..."
if [ -d "$DIRECTORY/.git" ]; then
    source ${WORKINGDIR}/git_pull.sh
fi

source ${WORKINGDIR}/activate.sh

source ${WORKINGDIR}/cleanup.sh

