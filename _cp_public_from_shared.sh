#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Copy public FROM shared${NORMAL}"

DELDIR=( "public/css" "public/js" "public/fonts" "public/images" "public/webfonts" )

for dname in "${DELDIR[@]}"
do
  if [ -d "$DIRECTORY/$dname" ]; then
    echo "Removing $DIRECTORY/$dname"
    rm -rf "${DIRECTORY:?}/$dname"
  fi
done

echo "Syncing public directory from $SHAREDDIR to $DIRECTORY/public"
rsync -a "$SHAREDDIR/public" "$DIRECTORY/"
