#!/usr/bin/env bash
if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"

    source ./_setup.sh
fi

echo -e "${GREEN}Permissions${NORMAL}"

ROGROUP=( "app" "bootstrap" "public" "vendor" "config" "resources" "local" "routes" )
RWGROUP=( "storage" "bootstrap/cache" )

RODIR=750
RWDIR=770
ROFILE=640
RWFILE=660

if [ ! -z "$USER" ]; then
    echo "Changing ownership of \"${BASEDIR}\":     user => \"${USER}\""
    chown -R ${USER} ${BASEDIR}
fi

echo "Changing ownership of \"${DIRECTORY}/.env\":"
echo "    user => \"${USER}\"    group => \"${GROUP}\""
chown -R ${USER}:${GROUP} "${DIRECTORY}/.env"

echo "Changing ownership of \"${SHAREDDIR}/.env\":"
echo "    user => \"${USER}\"    group => \"root\""
chown -R ${USER}:root "${SHAREDDIR}/.env"

echo "Changing permissions of \"${DIRECTORY}/.env\" to \"rw_r_____\""
chmod ${ROFILE} "${DIRECTORY}/.env"

echo "Changing permissions of \"${SHAREDDIR}/.env\" to \"rw_______\""
chmod 600 "${SHAREDDIR}/.env"

if [ -f ${SHAREDDIR}/apache.conf ]; then
  echo "Changing ownership of \"${SHAREDDIR}/apache.conf\":"
  echo "    user => \"${USER}\"    group => \"${GROUP}\""
  chown -R ${USER}:${GROUP} "${SHAREDDIR}/apache.conf"
  echo "Changing permissions of \"${DIRECTORY}/apache.conf\" to \"rw_r_____\""
  chmod ${ROFILE} "${SHAREDDIR}/apache.conf"
fi

for x in "${ROGROUP[@]}"; do
    CURR=${DIRECTORY}/${x}
    CURRSH=${SHAREDDIR}/${x}
    if [ ! -d "${CURR}" ]; then
        echo "[${CURR}] does not exist... creating..."
        mkdir -p ${CURR}
    fi
    echo "Changing ownership of \"${CURR}\":"
    echo "    user => \"${USER}\"     group => \"${GROUP}\""
    chown -R ${USER}:${GROUP} ${CURR}

    echo "Changing permissions of directories in \"${CURR}\" to \"rwxr_x___\""
    find ${CURR} -type d -exec chmod ${RODIR} '{}' \;

    echo "Changing permissions of files in \"${CURR}\" to \"rw_r_____\""
    find ${CURR} -type f -exec chmod ${ROFILE} '{}' \;

    if [ -d "${CURRSH}" ]; then
        echo "Changing ownership of \"${CURRSH}\":"
        echo "    user => \"${USER}\"     group => \"root\""
        chown -R ${USER}:root ${CURRSH}

        echo "Changing permissions of directories in \"${CURRSH}\" to \"rwxr_x___\""
        find ${CURRSH} -type d -exec chmod ${RODIR} '{}' \;

        echo "Changing permissions of files in \"${CURRSH}\" to \"rw_r_____\""
        find ${CURRSH} -type f -exec chmod ${ROFILE} '{}' \;
    fi
done

for x in "${RWGROUP[@]}"; do
    CURR=${DIRECTORY}/${x}
    CURRSH=${SHAREDDIR}/${x}
    if [ ! -d "${CURR}" ]; then
        echo "[${CURR}] does not exist... creating..."
        mkdir -p ${CURR}
    fi
    echo "Changing ownership of \"${CURR}\":"
    echo "    user => \"${USER}\"     group => \"${GROUP}\""
    chown -R ${USER}:${GROUP} ${CURR}

    echo "Changing permissions of directories in \"${CURR}\" to \"rwxrwx___\""
    find ${CURR} -type d -exec chmod ${RWDIR} '{}' \;

    echo "Changing permissions of files in \"${CURR}\" to \"rw_rw____\""
    find ${CURR} -type f -exec chmod ${RWFILE} '{}' \;

    if [ -d "${CURRSH}" ]; then
        echo "Changing ownership of \"${CURRSH}\":"
        echo "    user => \"${USER}\"     group => \"root\""
        chown -R ${USER}:root ${CURRSH}

        echo "Changing permissions of directories in \"${CURRSH}\" to \"rwxrwx___\""
        find ${CURRSH} -type d -exec chmod ${RWDIR} '{}' \;

        echo "Changing permissions of files in \"${CURRSH}\" to \"rw_rw____\""
        find ${CURRSH} -type f -exec chmod ${RWFILE} '{}' \;
    fi
done
