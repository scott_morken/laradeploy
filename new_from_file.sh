#!/usr/bin/env bash
if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"

    source ./_setup.sh
fi
if [ -z $FILE ]; then
    err "File name is not provided.  Please specify the file path and name with -f="
    exit 1
fi
if [ ! -f $FILE ]; then
    err "${FILE} does not exist."
    exit 1
fi

echo -e "${GREEN}Creating new release from ${FILE}${NORMAL}"

source ${WORKINGDIR}/new_release.sh

RELEASE=${NEWRELEASENAME}

DIRECTORY=${NEWRELEASEDIR}

echo -e "Untarring file..."
tar zxf "$FILE" -C "$DIRECTORY"

source ${WORKINGDIR}/_overwrite_new_to_shared.sh

source ${WORKINGDIR}/_overwrite_from_shared.sh

source ${WORKINGDIR}/_cp_files_from_shared.sh

echo -e "${BLUE}Ready to activate release ${RELEASE}${NORMAL}"
