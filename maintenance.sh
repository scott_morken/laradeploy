#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"

    source ./_setup.sh
fi

echo -e "${GREEN}Maintenance${NORMAL}"

if [ -d ${DOCROOT} ] && [ -L ${DOCROOT} ]; then
    unlink "${DOCROOT}"
elif [ -d ${DOCROOT} ] && [ ! -L ${DOCROOT} ]; then
    echo -e "${RED}${DOCROOT} exists but cannot be linked, is it a real directory?${NORMAL}"
    exit 1
fi

slink "${WORKINGDIR}/maintenance" "${DOCROOT}"
