#!/usr/bin/env bash
if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"

    source ./_setup.sh
fi

echo -e "${GREEN}Git pull${NORMAL}"

echo "Switching to master branch..."
cd ${DIRECTORY}
${GITCMD} checkout ${GITBRANCH}
${GITCMD} fetch ${GITNAME}
${GITCMD} merge ${GITNAME}/${GITBRANCH}
