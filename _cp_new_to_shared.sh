#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Copy new TO shared${NORMAL}"

COPYFILE=( ".env" "apache.conf" )

# copy public (not .copy) since built files are in copy
DELDIR=( "public/css" "public/js" "public/fonts" "public/webfonts" )

for dname in "${DELDIR[@]}"
do
  if [ -d "$DIRECTORY/$dname" ]; then
    if [ -d "$SHAREDDIR/$dname" ]; then
      echo "Removed old shared directory $SHAREDDIR/$dname"
      rm -rf "${SHAREDDIR:?}/$dname"
    fi
  fi
done

source ${WORKINGDIR}/_cp_public_to_shared.sh

source ${WORKINGDIR}/_cp_config_copy.sh

source ${WORKINGDIR}/_cp_resources_copy.sh

for fname in "${COPYFILE[@]}"
do
  if [ ! -f ${SHAREDDIR}/${fname} ]; then
    if [ -f ${DIRECTORY}/${fname} ]; then
      echo "Copying $DIRECTORY/$fname to $SHAREDDIR"
      cp -a "$DIRECTORY/$fname" "$SHAREDDIR/$fname"
    else
      touch "$SHAREDDIR/$fname"
    fi
  fi
done
