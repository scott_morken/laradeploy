#!/usr/bin/env bash

print_help() {
    echo "------------------------------------------"
    echo "Usage:"
    echo " command -r=release [-n=name] [-e=environment] [-g=group] [-u=user] [-d=docroot] [f=file] [-v=gitpath] [-y]"
    echo " command --release=release [--name=name] [--env=environment] [--group=group] [--user=user] [--docroot=docroot] [--vcs=gitremote] [-y]"
    echo "  > release should be the name of the releases/{release} directory where app is installed"
    echo "  > name should be the name of the site (default is 'default')"
    echo "  > docroot should be the document root of the site (default is '/www/{name}/{env}/current')"
    echo "  > env should be the environment, prod or dev (default is 'dev')"
    echo "  > user should be the user owner of the instance (default is 'deploy')"
    echo "  > group should be the group owner of the instance (default is 'www-data')"
    echo "  > file should be the path and file name of a tar.gz file"
    echo "  > gitpath should be the git clone URI"
    echo "  > -y causes the command to run without confirmation(s)"
    echo "------------------------------------------"
}

rrm() {
    if [[ -e ${1} ]] && [[ ! -z ${2} ]]; then
        echo "Removing ${UNDERLN}${2}${NORMAL} from ${UNDERLN}${1}${NORMAL}..."
        find ${1} -name "${2}" -print0 | xargs -0 rm -rf
    fi
}
get_release() {
    if [ -z "${1}" ]; then
        echo "No release specified, checking ${RELEASEDIR}..."
        last_file="$(ls ${RELEASEDIR} | tail -n 1)"
        if [ "${last_file:-0}" != 0 ]; then
        echo -e "${GREEN}  Found release: ${last_file}...${NORMAL}"
            RELEASE=${last_file}
        fi
    fi
}
is_valid_env() {
    if [ -z "${1}" ] || [[ ! -d "${1}" ]]; then
        err "${1} is not a valid environment"
        print_help
        exit 1
    fi
}
is_user() {
if [ -z "${1}" ] || [[ $(id -un "${1}" 2> /dev/null) != "${1}" ]]; then
    err "User ${1} does not exist"
    print_help
    exit 1
fi
}

is_group() {
if ! grep -q -E "^${1}:" /etc/group; then
    err "Group ${1} does not exist"
    print_help
    exit 1
fi
}

is_laravel() {
if [ -z "${1}" ] || [ ! -d "${1}/storage" ] || [ ! -f "${1}/.env" ]; then
    err "Please provide a valid Laravel path: ${1} does not exist."
    print_help
    exit 1
fi
}

err() {
    echo -e "${RED}** ERROR: ${1} **${NORMAL}"
}

slink() {
    echo "Checking link from ${1} to ${2}..."
    if [ -L ${2} ] && [ ! -e ${2} ]; then
        err "${2} is a bad sym link, unlinking..."
        unlink ${2}
    fi
    if [ ! -L ${2} ] && [ ! -e ${2} ]; then
        echo "  Linking..."
        if [ -e ${1} ]; then
            ln -vs ${1} ${2}
        else
            err "${1} does not exist."
        fi
    else
        echo -e "${BLUE}  ${2} exists...${NORMAL}"
    fi
}

ul() {
    echo "Checking for link ${UNDERLN}${1}${NORMAL}..."
    if [ -L ${1} ]; then
        echo "  Removing sym link ${UNDERLN}${1}${NORMAL}"
        unlink ${1}
    else
        echo "  No link found"
    fi
}

c_d() {
    local orig=$1
    local shared=$2
    local cporig=$orig
    echo "Copy and delete for ${orig}:${shared}"
    if [ -L ${orig} ]; then
        if [ ! -e ${orig} ]; then
            err "${orig} is a bad link, unlinking..."
            unlink ${orig}
        else
            echo -e "${BLUE}  ${orig} is already a sym link, skipping...${NORMAL}"
        fi
        return 0
    fi
    if [ -e ${orig} ]; then
        echo "  Copying from ${orig} to ${shared}"
        if [ -d ${orig} ]; then
            cporig="${orig}/."
        fi
        cp -a ${cporig} ${shared}
        echo "  Removing ${orig}"
        confirm && rm -rf ${orig}
    else
        err "${orig} does not exist"
    fi
}

confirm() {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case $response in
        [yY][eE][sS]|[yY])
            true
            ;;
        *)
            false
            ;;
    esac
}
