#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Copy TO shared${NORMAL}"

COPYDIR=( "resources" "config" )

source ${WORKINGDIR}/_cp_public_to_shared.sh

for dname in "${COPYDIR[@]}"
do
  echo "Copying $DIRECTORY/$dname to $SHAREDDIR"
  cp -a "$DIRECTORY/$dname/." "$SHAREDDIR/$dname"
done
