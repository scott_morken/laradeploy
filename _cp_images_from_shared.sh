#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Copy public/images FROM shared${NORMAL}"

echo "Copying image directory from $SHAREDDIR/public/images to $DIRECTORY/public/images"
cp -a "$SHAREDDIR/public/images/." "$DIRECTORY/public/images/"

echo "Copying *.png from $SHAREDDIR/public to $DIRECTORY/public"
cp -a "$SHAREDDIR/public/*.png" "$DIRECTORY/public/"

echo "Copying *.ico from $SHAREDDIR/public to $DIRECTORY/public"
cp -a "$SHAREDDIR/public/*.ico" "$DIRECTORY/public/"
