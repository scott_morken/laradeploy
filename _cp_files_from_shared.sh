#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Copy files FROM shared${NORMAL}"

COPYFILE=( ".env" "apache.conf" )

for fname in "${COPYFILE[@]}"
do
  echo "Copying $SHAREDDIR/$fname to $DIRECTORY"
  cp -a "$SHAREDDIR/$fname" "$DIRECTORY/$fname"
done

if [ -f "$SHAREDDIR/favicon.ico" ]; then
  echo "Copying favicon"
  cp -a "$SHAREDDIR/favicon.ico" "$DIRECTORY/public"
fi
