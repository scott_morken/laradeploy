#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"

    source ./_setup.sh
fi

echo -e "${GREEN}Copy Shared${NORMAL}"

source ${WORKINGDIR}/_cp_from_shared.sh
