#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Directories${NORMAL}"

if [ ! -d "$SHAREDDIR" ]; then
    echo "Creating shared directory: ${SHAREDDIR}..."
    mkdir -p ${SHAREDDIR}
    mkdir -p ${SHAREDDIR}/{config,public,resources}
    chown -R ${USER}:${GROUP} ${SHAREDDIR}
    chmod -R 0750 ${SHAREDDIR}
else
    echo "Directory ${SHAREDDIR} exists..."
fi

if [ ! -d "$BACKUPDIR" ]; then
    echo "Creating backups directory: ${BACKUPDIR}..."
    mkdir -p ${BACKUPDIR}
    chown -R ${USER}:${GROUP} ${BACKUPDIR}
    chmod -R 0770 ${BACKUPDIR}
else
    echo "Directory ${BACKUPDIR} exists..."
fi

if [ ! -d "$RELEASEDIR" ]; then
    echo "Creating releases directory: ${RELEASEDIR}..."
    mkdir -p ${RELEASEDIR}
    chown -R ${USER}:${GROUP} ${RELEASEDIR}
    chmod -R 0750 ${RELEASEDIR}
else
    echo "Directory ${RELEASEDIR} exists..."
fi
