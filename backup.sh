#!/usr/bin/env bash
if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"

    source ./_setup.sh
fi

source ${WORKINGDIR}/_directories.sh

echo -e "${GREEN}Backup${NORMAL}"

TARGETSITEBACKUP="${BACKUPDIR}/${RELEASE}.site.tar.gz"

echo "Backup ${DIRECTORY} to ${TARGETSITEBACKUP}"
/bin/tar --exclude=".env" -zcf ${TARGETSITEBACKUP} ${DIRECTORY}
ls -l ${TARGETSITEBACKUP}

TARGETSHAREDBACKUP="${BACKUPDIR}/${RELEASE}.shared.tar.gz"

echo "Backup ${SHAREDDIR} to ${TARGETSHAREDBACKUP}"
/bin/tar --exclude=".env" -zcf ${TARGETSHAREDBACKUP} ${SHAREDDIR}
ls -l ${TARGETSHAREDBACKUP}

chown -R ${USER}:${USER} ${BACKUPDIR}
