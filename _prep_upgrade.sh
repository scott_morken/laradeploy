#!/usr/bin/env bash

source ${WORKINGDIR}/new_release.sh

echo "Copying original ${UNDERLN}${DIRECTORY}${NORMAL} to ${UNDERLN}${NEWRELEASEDIR}${NORMAL}..."
cp -a ${DIRECTORY}/. ${NEWRELEASEDIR}

DIRECTORY=${NEWRELEASEDIR}
