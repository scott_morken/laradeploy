#!/usr/bin/env bash
if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

if [ -z $GITPATH ]; then
    err "Remote server is not not provided. Please provide the git repo path."
    exit 1
fi

echo -e "${GREEN}Git clone from ${GITPATH}${NORMAL}"

source ${WORKINGDIR}/new_release.sh

cd ${RELEASEDIR}

${GITCMD} clone ${GITPATH} ${NEWRELEASENAME}

cd ${NEWRELEASEDIR}

${GITCMD} checkout ${GITBRANCH}

RELEASE=${NEWRELEASENAME}

source ${WORKINGDIR}/_cp_from_shared.sh

source ${WORKINGDIR}/activate.sh

source ${WORKINGDIR}/cleanup.sh

