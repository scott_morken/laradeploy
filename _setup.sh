#!/usr/bin/env bash

source ./_env.sh
source ${WORKINGDIR}/_functions.sh

for i in "$@"
do
case $i in
    -f=*|--file=*)
    FILE="${i#*=}"
    shift
    ;;
    -r=*|--release=*)
    RELEASE="${i#*=}"
    shift # past argument=value
    ;;
    -n=*|--name=*)
    NAME="${i#*=}"
    shift # past argument=value
    ;;
    -e=*|--env=*)
    ENV="${i#*=}"
    shift # past argument=value
    ;;
    -g=*|--group=*)
    GROUP="${i#*=}"
    shift # past argument=value
    ;;
    -u=*|--user=*)
    USER="${i#*=}"
    shift # past argument=value
    ;;
    -d=*|--docroot=*)
    DOCROOT="${i#*=}"
    shift # past argument=value
    ;;
    -v=*|-vcs=*)
    GITPATH="${i#*=}"
    shift # past argument=value
    ;;
    -y)
    FORCE=1
    shift # past argument=value
    ;;
    *)
            # unknown option
    ;;
esac
done

if [ $(id -u) != 0 ]; then
    err "You must run this with sudo or root"
    exit 1
fi

BASEDIR="${root}/${NAME}/${ENV}"
SHAREDDIR="${BASEDIR}/shared"
RELEASEDIR="${BASEDIR}/releases"
BACKUPDIR="${BASEDIR}/backups"

CURRDATETIME=`date +"%Y%m%d%H%M"`

get_release ${RELEASE}
DIRECTORY="${RELEASEDIR}/${RELEASE}"

if [[ ${MINCHECKS} -eq 0 ]]; then
    echo "Validating environment..."
    is_user ${USER}
    is_group ${GROUP}
fi

if [[ -z ${DOCROOT} ]]; then
    DOCROOT="${BASEDIR}/current"
fi


echo -e "${GREEN}<<< Init Info >>>${NORMAL}"
echo -e "  ${BLUE}> Laravel directory:${NORMAL} ${DIRECTORY}"
echo -e "  ${BLUE}> App name:${NORMAL} ${NAME}"
echo -e "  ${BLUE}> Environment:${NORMAL} ${ENV}"
echo -e "  ${BLUE}> Document root:${NORMAL} ${DOCROOT}"
echo -e "  ${BLUE}> User:${NORMAL} ${USER}"
echo -e "  ${BLUE}> Group:${NORMAL} ${GROUP}"
echo -e "  ${BLUE}> Release dir:${NORMAL} ${RELEASEDIR}"
echo -e "  ${BLUE}> Shared dir:${NORMAL} ${SHAREDDIR}"
echo -e "  ${BLUE}> Backup dir:${NORMAL} ${BACKUPDIR}"
echo -e "${GREEN}<<< End Init Info >>>${NORMAL}"

if [[ -z ${FORCE} ]]; then
  echo -e "${RED}Continuing will make changes to your application environment!${NORMAL}"
  confirm && echo -e "${BLUE}Let's do this!${NORMAL}" || exit 0
fi
