#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"

    source ./_setup.sh
fi

source ${WORKINGDIR}/_cp_new_to_shared.sh
