#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

COPY="config"

echo -e "${GREEN}Copy $DIRECTORY/$COPY.copy${NORMAL}"

if [ ! -d "$DIRECTORY/$COPY.copy" ]; then
  echo -r "${RED}$DIRECTORY/$COPY.copy does not exist!${NORMAL}"
  exit 1
fi

echo "Note that the config.copy does not delete existing config files"

echo "Copying $DIRECTORY/$COPY.copy to $SHAREDDIR/$COPY"
cp -a "$DIRECTORY/$COPY.copy/." "$SHAREDDIR/$COPY"

echo "Copying $DIRECTORY/$COPY.copy to $DIRECTORY/$COPY"
cp -a "$DIRECTORY/$COPY.copy/." "$DIRECTORY/$COPY"
