#!/usr/bin/env bash
if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Cleanup${NORMAL}"

rrm ${RELEASE} "*.tar"
rrm ${RELEASE} "*.tar.gz"

LS=$(ls -d ${RELEASEDIR}/*/ | head -n -3)
if [[ "${LS:-0}" != 0 ]]; then
    if [[ -n $FORCE ]] || confirm "${BLUE}Remove old releases? [y\N]${NORMAL} "; then
        echo "Removing: "
        echo "${LS}"
        ls -d ${RELEASEDIR}/*/ | head -n -3 | xargs rm -rf
    fi
fi

LS=$(ls -d ${BACKUPDIR}/*.site.tar.gz | head -n -3)
if [ "${LS:-0}" != 0 ]; then
    if [[ -n $FORCE ]] || confirm "${BLUE}Remove old site tarballs? [y\N]${NORMAL} "; then
        echo "Removing: "
        echo "${LS}"
        ls -d ${BACKUPDIR}/*.site.tar.gz | head -n -3 | xargs rm
    fi
fi

LS=$(ls -d ${BACKUPDIR}/*.shared.tar.gz | head -n -3)
if [ "${LS:-0}" != 0 ]; then
    if [[ -n $FORCE ]] || confirm "${BLUE}Remove old shared tarballs? [y\N]${NORMAL} "; then
        echo "Removing: "
        echo "${LS}"
        ls -d ${BACKUPDIR}/*.shared.tar.gz | head -n -3 | xargs rm
    fi
fi
