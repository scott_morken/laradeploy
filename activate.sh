#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"

    source ./_setup.sh
fi

source ${WORKINGDIR}/_directories.sh
source ${WORKINGDIR}/permissions.sh

if [ -e "${DIRECTORY}/_finalize.sh" ]; then
    source ${DIRECTORY}/_finalize.sh
fi

echo -e "${GREEN}Activate${NORMAL}"

if [ -d ${DOCROOT} ] && [ -L ${DOCROOT} ]; then
    unlink "${DOCROOT}"
elif [ -d ${DOCROOT} ] && [ ! -L ${DOCROOT} ]; then
    echo -e "${RED}${DOCROOT} exists but cannot be linked, is it a real directory?${NORMAL}"
    exit 1
fi

slink "${DIRECTORY}" "${DOCROOT}"
