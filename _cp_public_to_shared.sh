#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Copy public TO shared${NORMAL}"

echo "Syncing public directory from $DIRECTORY/public to $SHAREDDIR"
rsync -a "$DIRECTORY/public" "$SHAREDDIR/" --exclude "public/images"
