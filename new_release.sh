#!/usr/bin/env bash
if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"

    source ./_setup.sh
fi

source ${WORKINGDIR}/_directories.sh

echo -e "${GREEN}Creating new release dir${NORMAL}"

if [[ -z ${NEWRELEASENAME} ]]; then
    NEWRELEASENAME=`date +"%Y%m%d%H%M"`
fi
NEWRELEASEDIR=${RELEASEDIR}/${NEWRELEASENAME}

echo -e "New release dir: ${NEWRELEASEDIR}"

if [ ! -d ${NEWRELEASEDIR} ]; then
  mkdir -p ${NEWRELEASEDIR}
fi

if [ ! -z "$USER" ]; then
    echo "Changing ownership of \"${NEWRELEASEDIR}\":     user => \"${USER}\""
    chown -R ${USER} ${NEWRELEASEDIR}
fi
