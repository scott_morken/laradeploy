#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Overwrite new TO shared${NORMAL}"

COPYFILE=( ".env" )

source ${WORKINGDIR}/_overwrite_config_copy.sh

source ${WORKINGDIR}/_overwrite_resources_copy.sh

source ${WORKINGDIR}/_overwrite_public.sh

for fname in "${COPYFILE[@]}"
do
  if [ ! -f ${SHAREDDIR}/${fname} ]; then
    if [ -f ${DIRECTORY}/${fname} ]; then
      echo "Copying $DIRECTORY/$fname to $SHAREDDIR"
      cp -a "$DIRECTORY/$fname" "$SHAREDDIR/$fname"
    else
      touch "$SHAREDDIR/$fname"
    fi
  fi
done
