#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Overwrite FROM shared${NORMAL}"

COPYDIR=( "resources" "config" "public" )

for dname in "${COPYDIR[@]}"
do
  if [ -d "$DIRECTORY/$dname" ]; then
    echo "Removing $DIRECTORY/$dname"
    rm -rf "${DIRECTORY:?}/$dname"
  fi
  echo "Copying $SHAREDDIR/$dname to $DIRECTORY"
  cp -a "$SHAREDDIR/$dname/." "$DIRECTORY/$dname"
done
