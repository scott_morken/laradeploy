#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

COPY="public"

echo -e "${GREEN}Copy $DIRECTORY/$COPY${NORMAL}"

if [ -d "$SHAREDDIR/$COPY" ]; then
  echo "Removing old shared directory $SHAREDDIR/$COPY"
  rm -rf "${SHAREDDIR:?}/$COPY"
fi

echo "Copying $DIRECTORY/$COPY to $SHAREDDIR/$COPY"
cp -a "$DIRECTORY/$COPY/." "$SHAREDDIR/$COPY"
