#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Copy FROM shared${NORMAL}"

COPYDIR=( "resources" "config" )

COPYFILE=( ".env" "apache.conf" )

for dname in "${COPYDIR[@]}"
do
  if [ -d "$DIRECTORY/$dname" ]; then
    echo "Removing $DIRECTORY/$dname"
    rm -rf "${DIRECTORY:?}/$dname"
  fi
  echo "Copying $SHAREDDIR/$dname to $DIRECTORY"
  cp -a "$SHAREDDIR/$dname/." "$DIRECTORY/$dname"
done

source ${WORKINGDIR}/_cp_public_from_shared.sh
source ${WORKINGDIR}/_cp_files_from_shared.sh
