#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

COPY="resources"

echo -e "${GREEN}Copy $DIRECTORY/$COPY.copy${NORMAL}"

if [ ! -d "$DIRECTORY/$COPY.copy" ]; then
  echo -r "${RED}$DIRECTORY/$COPY.copy does not exist!${NORMAL}"
  exit 1
fi

if [ -d "$SHAREDDIR/$COPY" ]; then
  echo "Removing old shared directory $SHAREDDIR/$COPY"
  rm -rf "${SHAREDDIR:?}/$COPY"
fi

if [ -d "$DIRECTORY/$COPY" ]; then
  echo "Removing old directory $DIRECTORY/$COPY"
  rm -rf "${DIRECTORY:?}/$COPY"
fi

echo "Copying $DIRECTORY/$COPY.copy to $SHAREDDIR/$COPY"
cp -a "$DIRECTORY/$COPY.copy/." "$SHAREDDIR/$COPY"

echo "Copying $DIRECTORY/$COPY.copy to $DIRECTORY/$COPY"
cp -a "$DIRECTORY/$COPY.copy/." "$DIRECTORY/$COPY"
