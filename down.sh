#!/usr/bin/env bash

if [ -z $NAME ]; then
    set -e

    cd "$(dirname "$0")"
    MINCHECKS=1
    source ./_setup.sh
fi

echo -e "${GREEN}Maintenance mode${NORMAL}"

if [ -e $DOCROOT ]; then
    if [ -L $DOCROOT ]; then
        unlink ${DOCROOT}
    else
        err "$DOCROOT is not a symbolic link, not downing site"
        exit 1
    fi
fi

slink "${root}/maintenance" "${DOCROOT}"
